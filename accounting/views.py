# You will probably need more methods from flask but this one is a good start.
from flask import render_template, Response
import json
# Import things from Flask that we need.
from accounting import app, db

# Import our models
from accounting.utils import PolicyAccounting
from models import Contact, Invoice, Policy
from datetime import datetime
from flask import abort


# Routing for the server.
@app.route("/")
def index():
    # You will need to serve something up here.
    return render_template('index.html')


@app.route("/api/policies")
def api_policies():
    policies = []
    for policy in Policy.query.all():
        policies.append({
            "id": policy.id,
            "policy_number": policy.policy_number,
            "billing_schedule": policy.billing_schedule,
            "effective_date": policy.effective_date.strftime('%Y-%m-%d'),
            "status": policy.status,
            "annual_premium": policy.annual_premium
        })
    return Response(json.dumps(policies), mimetype='application/json')


@app.route("/api/policies/<policy_number>/<date_cursor>")
def api_policies_single(policy_number, date_cursor):
    try:
        date_cursor = datetime.strptime(date_cursor, '%Y-%m-%d').date()
    except:
        abort(422)

    try:
        policy = Policy.query.filter_by(policy_number=policy_number).one()
    except:
        abort(404)

    pa = PolicyAccounting(policy.id)
    invoices = []
    for invoice in policy.invoices:
        invoices.append({
            "bill_date": invoice.bill_date.strftime('%Y-%m-%d'),
            "due_date": invoice.due_date.strftime('%Y-%m-%d'),
            "cancel_date": invoice.due_date.strftime('%Y-%m-%d'),
            "amount_due": invoice.amount_due
        })
    data = {
        "id": policy.id,
        "policy_number": policy.policy_number,
        "billing_schedule": policy.billing_schedule,
        "effective_date": policy.effective_date.strftime('%Y-%m-%d'),
        "status": policy.status,
        "annual_premium": policy.annual_premium,
        "balance": pa.return_account_balance(date_cursor),
        "cancellation_pending": pa.evaluate_cancellation_pending_due_to_non_pay(date_cursor),
        "invoices": invoices
    }
    return Response(json.dumps(data), mimetype='application/json')
