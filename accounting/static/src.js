var Policy = function (policy) {
    return Object.assign(policy)
};

var PolicyListModel = function () {
    var self = this;

    self.policy_number = ko.observable("");
    self.date = ko.observable("");
    self.policy_found = ko.observable(false);

    self.policies = ko.observableArray([]);

    self.error = ko.observable("");

    self.searchPolicy = function () {
        // Safari is still missing native input validator
        if (!this.policy_number()) {
            self.error("Policy number is required");
            return
        }
        if (!this.date()) {
            self.error("Date is required");
            return
        }
        fetch(['api', 'policies', this.policy_number(), this.date()].join('/'))
            .then(function (response) {
                if (!response.ok) {
                    console.log(response.status);
                    switch (response.status) {
                        case 404:
                            self.error("Policy number not found");
                            break;
                        case 422:
                            self.error("Date format is invalid");
                            break
                    }
                    throw Error(response.statusText);
                }
                return response;
            })
            .then(function (response) {
                return response.json();
            })
            .then(function (policy) {
                self.error('');
                self.policies.removeAll();
                self.policies.push(new Policy(policy));
                self.policy_found(true);
            })
            .catch(function (error) {
                console.error(error)
            });
    };

    self.selectPolicy = function (policy) {
        self.policy_number(policy.policy_number);
        self.date(new Date().toISOString().slice(0, 10));
        self.searchPolicy()
    };

    self.clearSearch = function () {
        self.policy_number('');
        self.date('');
        self.policy_found(false);
        self.error('');
        self.fetchPolicies()
    };

    self.removeError = function () {
        self.error('');
    };

    self.fetchPolicies = function () {
        fetch('/api/policies')
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                self.policies.removeAll();
                json.forEach(function (policy) {
                    self.policies.push(new Policy(policy))
                });
            });
    };

    self.fetchPolicies()
};

var bindings = new PolicyListModel();

ko.applyBindings(bindings);

window.bindings = bindings;