#!/user/bin/env python2.7

import unittest
from datetime import date, datetime, timedelta

from accounting import db
from models import Contact, Invoice, Payment, Policy
from utils import PolicyAccounting

"""
#######################################################
Test Suite for Accounting
#######################################################
"""


class TestBillingSchedules(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_agent = Contact('Test Agent', 'Agent')
        cls.test_insured = Contact('Test Insured', 'Named Insured')
        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.commit()

        cls.policy = Policy('Test Policy', date(2015, 1, 1), 1200)
        db.session.add(cls.policy)
        cls.policy.named_insured = cls.test_insured.id
        cls.policy.agent = cls.test_agent.id
        db.session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.policy)
        db.session.commit()

    def setUp(self):
        pass

    def tearDown(self):
        for invoice in self.policy.invoices:
            db.session.delete(invoice)
        db.session.commit()

    def test_annual_billing_schedule(self):
        self.policy.billing_schedule = "Annual"
        # No invoices currently exist
        self.assertFalse(self.policy.invoices)
        # Invoices should be made when the class is initiated
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(len(self.policy.invoices), 1)
        self.assertEquals(self.policy.invoices[0].amount_due, self.policy.annual_premium)

    def test_monthly_billing_schedule(self):
        self.policy.billing_schedule = "Monthly"
        # No invoices currently exist
        self.assertFalse(self.policy.invoices)
        # Invoices should be made when the class is initiated
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(len(self.policy.invoices), 12)
        self.assertEquals(self.policy.invoices[0].amount_due, self.policy.annual_premium / 12)


class TestReturnAccountBalance(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_agent = Contact('Test Agent', 'Agent')
        cls.test_insured = Contact('Test Insured', 'Named Insured')
        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.commit()

        cls.policy = Policy('Test Policy', date(2015, 1, 1), 1200)
        cls.policy.named_insured = cls.test_insured.id
        cls.policy.agent = cls.test_agent.id
        db.session.add(cls.policy)
        db.session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.policy)
        db.session.commit()

    def setUp(self):
        self.payments = []

    def tearDown(self):
        for invoice in self.policy.invoices:
            db.session.delete(invoice)
        for payment in self.payments:
            db.session.delete(payment)
        db.session.commit()

    def test_annual_on_eff_date(self):
        self.policy.billing_schedule = "Annual"
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(pa.return_account_balance(date_cursor=self.policy.effective_date), 1200)

    def test_quarterly_on_eff_date(self):
        self.policy.billing_schedule = "Quarterly"
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(pa.return_account_balance(date_cursor=self.policy.effective_date), 300)

    def test_quarterly_on_last_installment_bill_date(self):
        self.policy.billing_schedule = "Quarterly"
        pa = PolicyAccounting(self.policy.id)
        invoices = Invoice.query.filter_by(policy_id=self.policy.id) \
            .order_by(Invoice.bill_date).all()
        self.assertEquals(pa.return_account_balance(date_cursor=invoices[3].bill_date), 1200)

    def test_quarterly_on_second_installment_bill_date_with_full_payment(self):
        self.policy.billing_schedule = "Quarterly"
        pa = PolicyAccounting(self.policy.id)
        invoices = Invoice.query.filter_by(policy_id=self.policy.id) \
            .order_by(Invoice.bill_date).all()
        self.payments.append(pa.make_payment(contact_id=self.policy.named_insured,
                                             date_cursor=invoices[1].bill_date, amount=600))
        self.assertEquals(pa.return_account_balance(date_cursor=invoices[1].bill_date), 0)

    def test_monthly_on_eff_date(self):
        self.policy.billing_schedule = "Monthly"
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(pa.return_account_balance(date_cursor=self.policy.effective_date), 100)

    def test_monthly_on_last_installment_bill_date(self):
        self.policy.billing_schedule = "Monthly"
        pa = PolicyAccounting(self.policy.id)
        invoices = Invoice.query.filter_by(policy_id=self.policy.id) \
            .order_by(Invoice.bill_date).all()
        self.assertEquals(pa.return_account_balance(date_cursor=invoices[11].bill_date), 1200)


class TestCancellationPending(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_agent = Contact('Test Agent', 'Agent')
        cls.test_insured = Contact('Test Insured', 'Named Insured')
        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.commit()

        cls.policy = Policy('Test Policy', date(2015, 1, 1), 1200)
        db.session.add(cls.policy)
        cls.policy.named_insured = cls.test_insured.id
        cls.policy.agent = cls.test_agent.id
        db.session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.policy)
        db.session.commit()

    def setUp(self):
        self.payments = []

    def tearDown(self):
        for invoice in self.policy.invoices:
            db.session.delete(invoice)
        for payment in self.payments:
            db.session.delete(payment)
        db.session.commit()

    def test_cancellation_pending_is_false_for_new_policies(self):
        self.policy.billing_schedule = 'Monthly'
        pa = PolicyAccounting(self.policy.id)
        self.assertFalse(pa.evaluate_cancellation_pending_due_to_non_pay(date(2015, 1, 1)))

    def test_cancellation_pending_is_false_before_due_date(self):
        self.policy.billing_schedule = 'Monthly'
        pa = PolicyAccounting(self.policy.id)
        self.assertFalse(pa.evaluate_cancellation_pending_due_to_non_pay(date(2015, 1, 15)))

    def test_cancellation_pending_is_true_one_week_after_due_date(self):
        self.policy.billing_schedule = 'Monthly'
        pa = PolicyAccounting(self.policy.id)
        self.assertTrue(pa.evaluate_cancellation_pending_due_to_non_pay(date(2015, 2, 8)))

    def test_cancellation_pending_is_false_two_weeks_after_due_date_with_a_payment(self):
        self.policy.billing_schedule = 'Monthly'
        pa = PolicyAccounting(self.policy.id)
        self.payments.append(pa.make_payment(amount=pa.return_account_balance(date(2015, 2, 1)),
                                             date_cursor=date(2015, 2, 1)))
        self.assertFalse(pa.evaluate_cancellation_pending_due_to_non_pay(date(2015, 2, 14)))

    def test_cancellation_pending_prevent_payment_from_non_agents(self):
        self.policy.billing_schedule = 'Monthly'
        pa = PolicyAccounting(self.policy.id)
        payment = pa.make_payment(amount=100, date_cursor=date(2015, 2, 14))
        if payment is not None:
            self.payments.append(payment)
            self.fail('No payment should be created')
        else:
            self.assertIsNone(payment)


class TestChangeBillingSchedule(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_agent = Contact('Test Agent', 'Agent')
        cls.test_insured = Contact('Test Insured', 'Named Insured')
        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.commit()

        cls.policy = Policy('Test Policy', date(2015, 1, 1), 1200)
        db.session.add(cls.policy)
        cls.policy.named_insured = cls.test_insured.id
        cls.policy.agent = cls.test_agent.id
        db.session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.policy)
        db.session.commit()

    def setUp(self):
        self.payments = []

    def tearDown(self):
        for invoice in self.policy.invoices:
            db.session.delete(invoice)
        for payment in self.payments:
            db.session.delete(payment)
        db.session.commit()

    def test_skip_process_if_new_value_is_not_different(self):
        self.policy.billing_schedule = 'Annual'
        pa = PolicyAccounting(self.policy.id)
        self.assertFalse(pa.change_billing_schedule('Annual'))

    def test_billing_schedule_changes(self):
        self.policy.billing_schedule = 'Quarterly'
        pa = PolicyAccounting(self.policy.id)
        self.assertTrue(pa.change_billing_schedule('Monthly'))
        self.assertEquals(pa.policy.billing_schedule, 'Monthly')

    def test_marks_invoices_as_deleted(self):
        self.policy.billing_schedule = 'Quarterly'
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(Invoice.query.filter_by(policy_id=self.policy.id).order_by(Invoice.bill_date).count(), 4)
        pa.change_billing_schedule('Monthly')
        self.assertEquals(Invoice.query.filter_by(policy_id=self.policy.id).order_by(Invoice.bill_date).count(), 12)

    def test_returns_correctly_account_balance(self):
        self.policy.billing_schedule = 'Quarterly'
        pa = PolicyAccounting(self.policy.id)
        first_invoice = Invoice.query.filter_by(policy_id=self.policy.id).order_by(Invoice.bill_date).first()
        self.payments.append(pa.make_payment(amount=first_invoice.amount_due,
                                             date_cursor=first_invoice.bill_date))
        self.assertEquals(pa.return_account_balance(date_cursor=date(2015, 1, 1)), 0)
        pa.change_billing_schedule('Monthly')
        self.assertEquals(pa.return_account_balance(date_cursor=date(2015, 1, 1)), -200)
        self.assertEquals(pa.return_account_balance(date_cursor=date(2015, 2, 1)), -100)
        self.assertEquals(pa.return_account_balance(date_cursor=date(2015, 3, 1)), 0)


class TestCancelPolicy(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_agent = Contact('Test Agent', 'Agent')
        cls.test_insured = Contact('Test Insured', 'Named Insured')
        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.commit()

        cls.policy = Policy('Test Policy', date(2015, 1, 1), 1200)
        db.session.add(cls.policy)
        cls.policy.named_insured = cls.test_insured.id
        cls.policy.agent = cls.test_agent.id
        db.session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.policy)
        db.session.commit()

    def setUp(self):
        self.payments = []

    def tearDown(self):
        for invoice in self.policy.invoices:
            db.session.delete(invoice)
        for payment in self.payments:
            db.session.delete(payment)
        db.session.commit()

    def test_status_changes(self):
        pa = PolicyAccounting(self.policy.id)
        pa.evaluate_cancel(date_cursor=date(2016, 1, 1))
        self.assertEquals(pa.policy.status, 'Canceled')

    def test_canceled_date_is_updated(self):
        pa = PolicyAccounting(self.policy.id)
        pa.evaluate_cancel(date_cursor=date(2016, 1, 1))
        self.assertEquals(pa.policy.canceled_date, date(2016, 1, 1))

    def test_canceled_reason_skips_check(self):
        pa = PolicyAccounting(self.policy.id)
        pa.evaluate_cancel(date_cursor=date(2016, 1, 1), canceled_reason='Overwriting')
        self.assertEquals(pa.policy.status, 'Canceled')
        self.assertEquals(pa.policy.canceled_date, date(2016, 1, 1))
        self.assertEquals(pa.policy.canceled_reason, 'Overwriting')
